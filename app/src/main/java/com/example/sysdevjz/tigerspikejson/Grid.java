package com.example.sysdevjz.tigerspikejson;

/**
 * Created by sysdevjz on 10/10/2016.
 */

public class Grid {
    private String image;
    private String title;
    private String tags;

    public Grid() {
        super();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public String getTags() {
        return tags;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
